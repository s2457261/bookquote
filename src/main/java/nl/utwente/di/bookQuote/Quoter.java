package nl.utwente.di.bookQuote;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;

public class Quoter {
    double getBookPrice(String isbn){
        double price = 0.0;
        isbn = (isbn == null )? "" : isbn;
        switch (isbn) {
            case "1":
                price = 10;
                break;

            case "2":
                price = 45.0;
                break;
            case "3":
                price = 20.0;
                break;
            case "4":
                price = 35.0;
                break;
            case "5":
                price = 50.0;
                break;
            default :
                price = 0;

        }
        return price;
    }

}
